// ==UserScript==
// @name        Drudge Report Enhancer
// @namespace   DrudgeReport
// @description Provides several enhancements to The Drudge Report
// @match       http://*.drudgereport.com/*
// @grant       none
// @author      Aaron Feledy
// @version     1.4
// ==/UserScript==

// Default settings
var settings = [
  '1.4',  // [0] Script version number
  '3',        // [1] Settings format version. Increment when settings format changes.
  '1',        // [2] Remove top ad
  '1',        // [3] Use countdown timer
  '180',      // [4] Refresh interval (in seconds)
  '1',        // [5] Notification sound: 1=fart, 2=boing, 3=pop, 0=off
  '1',        // [6] Siren notification sound: 1=on, 0=off
  '#ffffaa',  // [7] Unseen link background color
  '#777777',  // [8] Visted link color
  '1'         // [9] Open links in new tab
];

var countdownDiv = document.createElement('div'); //Declare here so it's available to all functions
var menuButtonDiv = document.createElement('div'); //Declare here so it's available to all functions

if (window === parent) { // Do not run in iframes

  // Check for settings cookie
  settingsCookie = getCookie('settings');
  if (settingsCookie) {
    var loadedSettings = CSVToArray(settingsCookie);
    // Reset settings if settings format has changed.
    if (loadedSettings[1] !== settings[1]) {
      createCookie('settings', settings, 10*356);
    } else {
      settings = loadedSettings;
    }
  } else {
    createCookie('settings', settings, 10*356);
  }

  // Define settings variables
  var removeAd            = settings[2];
  var countdownEnabled    = settings[3];
  var countDownInterval   = parseInt(settings[4]);
  var notificationSound   = parseInt(settings[5]);
  var sirenSound          = settings[6];
  var linkUnseenBg        = settings[7];
  var linkVisited         = settings[8];
  var linkTargetBlank     = settings[9];

  // Add page styles
  addStyle();
  
  // Remove top banner ad
  if (removeAd > 0){
    var ad = document.getElementsByTagName('center');
    if (ad[0]) ad[0].parentNode.removeChild(ad[0]);
  }

  // Stop existing auto refresh (cross browser compatible method)
  location.href = "javascript:(function(){ window.clearInterval(window.timer); })()";  

  // Add countdown element
  countdownDiv.id = 'countdown';
  countdownDiv.innerHTML = '';
  countdownDiv.style.position = 'fixed';
  countdownDiv.style.top = '0';
  countdownDiv.style.right = '30px';
  countdownDiv.style.zIndex = '50';
  countdownDiv.style.padding = '5px 10px';
  countdownDiv.style.fontSize = '1.5em';
  countdownDiv.style.textShadow = '-2px 1px 2px #bbb';
  countdownDiv.style.cursor = 'pointer';
  countdownDiv.style.WebkitTransition = 'all 500ms ease-in-out';
  countdownDiv.style.MozTransition = 'all 500ms ease-in-out';
  countdownDiv.style.MsTransition = 'all 500ms ease-in-out';
  countdownDiv.style.OTransition = 'all 500ms ease-in-out';
  countdownDiv.style.transition = 'all 500ms ease-in-out';
  document.body.appendChild(countdownDiv);

  // Start Countdown
  var countDownTime = countDownInterval + 1;
  countDown();

  // Check for previous link timestamp data
  var hashedLinksCookie = getCookie('hashedLinks');
  var hashedLinksPrev = [''];
  if (hashedLinksCookie) {
    hashedLinksPrev = CSVToArray(hashedLinksCookie, true);
  }

  // Cycle through current links
  var links = document.getElementsByTagName('a');
  var linkCount = links.length, hashedLinks = [], newLinks = 0;
  for (var i = 0; i < linkCount; i++) {
    if (links[i].pathname.length > 1 // ignore links to dot coms
      && !(isAd(links[i]))) // ignore ads
      {
      var img = links[i].getElementsByTagName('img');
      if (!(img[0])) { // ignore ads
        var linkHash = hash(links[i].href);
        hashedLinks.push(linkHash);
        links[i].setAttribute('hash',linkHash);
        // Should have many more than 5 unless there was no cookie or data is incomplete.
        // This will keep the script from highlighting everything in that situation.
        if (hashedLinksPrev.length > 5) {
          // Compare to previous links and highlight unseen
          if (hashedLinksPrev.indexOf(linkHash) == -1) {
            // Unseen link
            links[i].className = 'unseen';
            newLinks++;
          } else {
            links[i].className = 'seen';
          }
        }
      }
    }
    // Do this last to avoid isAd()
    if (linkTargetBlank > 0) links[i].setAttribute('target', '_blank'); //Open links in new tab
  }
  createCookie('hashedLinks', hashedLinks, 10*30);

  // Indicate new links in page title and play chime
  if (newLinks > 0) {
    var pageTitle = document.title
    document.title = '(' + newLinks + ') ' + pageTitle
    if (notificationSound) {
      chime = document.createElement('audio');
      chime.setAttribute('src',loadSound(notificationSound));
      chime.load();
      chime.play();
    }
  }

  // Detect siren headline and change favicon
  var siren = false;
  var images = document.getElementsByTagName('img');
  var imgCount = images.length;
  for (var ii = 0; ii < imgCount; ii++) {
    if (images[ii].src.indexOf('siren.gif') !== -1) {
      doSiren();
    }
  }
  
  // Add menu button
  menuButtonDiv.id = 'dre-menu-btn';
  menuButtonDiv.innerHTML = '<img alt="" src="data:image/gif;base64,R0lGODlhBQAUAPcBAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxNC0wMy0yMVQwODoxNzoyMC0wNTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTQtMDMtMjFUMDg6NDU6NDAtMDU6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMTQtMDMtMjFUMDg6NDU6NDAtMDU6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvZ2lmIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE3MUU4NTg2QjBGRjExRTM4M0EyQjFFNEZGQUI5NzVCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE3MUU4NTg3QjBGRjExRTM4M0EyQjFFNEZGQUI5NzVCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTcxRTg1ODRCMEZGMTFFMzgzQTJCMUU0RkZBQjk3NUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTcxRTg1ODVCMEZGMTFFMzgzQTJCMUU0RkZBQjk3NUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAABQAUAAAIGgABCBxIsKBBggESKlx4sKFDgwsjBnhIkWBAADs=" />';
  menuButtonDiv.style.position = 'fixed';
  menuButtonDiv.style.top = '0';
  menuButtonDiv.style.right = '0';
  menuButtonDiv.style.padding = '8px';
  menuButtonDiv.style.background = '#fff';
  menuButtonDiv.style.border = '1px solid #eee';
  menuButtonDiv.style.boxShadow = '-2px 1px 2px #bbb';
  menuButtonDiv.style.cursor = 'pointer';
  document.body.appendChild(menuButtonDiv);

  // Add menu elements
  var menuContainer = document.createElement('div');
  var menuOut = false;
  menuContainer.id = 'dre-menu-container';
  menuContainer.innerHTML = '';
  menuContainer.style.position = 'fixed';
  menuContainer.style.top = '40';
  menuContainer.style.right = '-250px';
  menuContainer.style.width = '228px';
  menuContainer.style.padding = '5px 10px';
  menuContainer.style.background = '#fff';
  menuContainer.style.fontFamily = '"Arial Narrow", Arial, sans-serif';
  menuContainer.style.border = '1px solid #ddd';
  menuContainer.style.boxShadow = '-2px 1px 2px #bbb';
  menuContainer.style.opacity = '0';
  menuContainer.style.WebkitTransition = 'all 500ms ease-in-out';
  menuContainer.style.MozTransition = 'all 500ms ease-in-out';
  menuContainer.style.MsTransition = 'all 500ms ease-in-out';
  menuContainer.style.OTransition = 'all 500ms ease-in-out';
  menuContainer.style.transition = 'all 500ms ease-in-out';
  document.body.appendChild(menuContainer);
  
  var menuHeader = document.createElement('div');
  menuHeader.innerHTML = '<p>Enhancer</p>';
  menuHeader.style.textAlign = 'center';
  menuHeader.style.fontWeight = 'bold';
  menuHeader.style.fontSize = '30px';
  menuHeader.style.fontStyle = 'italic';
  menuHeader.style.textShadow = '-2px 1px 2px #bbb';
  menuHeader.style.borderBottom = '1px solid #ddd';
  menuHeader.style.marginBottom = '0.4em';
  menuContainer.appendChild(menuHeader);
  
  // Build menu
  var optRemoveAd = document.createElement('a');
  var optCountDownInterval = document.createElement('a');
  var optNotificationSound = document.createElement('div');
  var optNotificationSound1 = document.createElement('a');
  var optNotificationSound2 = document.createElement('a');
  var optNotificationSound3 = document.createElement('a');
  var optNotificationSoundOff = document.createElement('a');
  var optSirenSound = document.createElement('a');
  menuContainer.appendChild(buildMenu());

}

/**********************
 * Event Listeners
 *********************/

// Countdown click listener
countdownDiv.onclick = function() {
  clearTimeout(counter);
  countDownTime = countDownInterval + 1;
  countdownDiv.style.color = 'black';
  countdownDiv.style.fontSize = '150%';
  countdownDiv.style.fontWeight = 'normal';
  countDown();
};

// Menu button click listener
menuButtonDiv.onclick = function() {
  if (menuOut) {
    menuContainer.style.right = '-250px';
    menuContainer.style.opacity = '0';
    menuOut = false;
  } else {
    menuContainer.style.right = '0';
    menuContainer.style.opacity = '1';
    menuOut = true;
  }
};

// Menu option click listeners
optRemoveAd.onclick = function() {
  if (removeAd == 1) {
    settings[2] = removeAd = 0;
    optRemoveAd.className = 'opt-off';
  } else {
    settings[2] = removeAd = 1;
    optRemoveAd.className = 'opt-on';
  }
  createCookie('settings', settings, 10*356);
};

optCountDownInterval.onclick = function() {
  var newCountDownInterval = prompt("Enter a new refresh interval in seconds (0 to disable):", countDownInterval);
  if (newCountDownInterval === '' || newCountDownInterval === null) {
    newCountDownInterval = countDownInterval;
  }
  if (newCountDownInterval % 1 === 0) {
    if (newCountDownInterval > 0) {
      settings[4] = countDownInterval = parseInt(newCountDownInterval);
      settings[3] = countdownEnabled = 1;
      optCountDownInterval.className = 'opt-on';
    } else {
      settings[4] = countDownInterval = 0;
      settings[3] = countdownEnabled = 0;
      optCountDownInterval.className = 'opt-off';
    }
    optCountDownInterval.innerHTML = 'Refresh every = ' + countDownInterval + ' seconds';
  } else {
    alert('Must be an integer!');
  }
  createCookie('settings', settings, 10*356);
  countdownDiv.click();
};

optNotificationSound1.onclick = function() {
  if (notificationSound != 1) {
    settings[5] = notificationSound = 1;
    optNotificationSound1.className = 'opt-on';
    optNotificationSound2.className = 'opt-off';
    optNotificationSound3.className = 'opt-off';
    optNotificationSoundOff.className = 'opt-off';
    // Play Sound
    chime = document.createElement('audio');
    chime.setAttribute('src',loadSound(notificationSound));
    chime.load();
    chime.play();
    // Update settings cookie
    createCookie('settings', settings, 10*356);
  }
};
optNotificationSound2.onclick = function() {
  if (notificationSound != 2) {
    settings[5] = notificationSound = 2;
    optNotificationSound1.className = 'opt-off';
    optNotificationSound2.className = 'opt-on';
    optNotificationSound3.className = 'opt-off';
    optNotificationSoundOff.className = 'opt-off';
    // Play Sound
    chime = document.createElement('audio');
    chime.setAttribute('src',loadSound(notificationSound));
    chime.load();
    chime.play();
    // Update settings cookie
    createCookie('settings', settings, 10*356);
  }
};
optNotificationSound3.onclick = function() {
  if (notificationSound != 3) {
    settings[5] = notificationSound = 3;
    optNotificationSound1.className = 'opt-off';
    optNotificationSound2.className = 'opt-off';
    optNotificationSound3.className = 'opt-on';
    optNotificationSoundOff.className = 'opt-off';
    // Play Sound
    chime = document.createElement('audio');
    chime.setAttribute('src',loadSound(notificationSound));
    chime.load();
    chime.play();
    // Update settings cookie
    createCookie('settings', settings, 10*356);
  }
};
optNotificationSoundOff.onclick = function() {
  if (notificationSound != 0) {
    settings[5] = notificationSound = 0;
    optNotificationSound1.className = 'opt-off';
    optNotificationSound2.className = 'opt-off';
    optNotificationSound3.className = 'opt-off';
    optNotificationSoundOff.className = 'opt-on';
    // Update settings cookie
    createCookie('settings', settings, 10*356);
  }
};

optSirenSound.onclick = function() {
  if (sirenSound == 1) {
    settings[5] = sirenSound = 0;
    optSirenSound.className = 'opt-off';
  } else {
    settings[5] = sirenSound = 1;
    optSirenSound.className = 'opt-on';
    siren = document.createElement('audio');
    siren.setAttribute('src',loadSound(4));
    siren.load();
    siren.play();
  }
  createCookie('settings', settings, 10*356);
};

/**********************
 * Functions
 *********************/
function buildMenu() {
  // Builds html for menu
  var optionsDiv = document.createElement('div');
  optionsDiv.innerHTML = '';
  optionsDiv.style.fontSize = '12px';
  optionsDiv.id = 'dre-menu';
  
  // Remove Ad Option
  if (removeAd == 1) {
    optRemoveAd.className = 'opt-on';
  } else {
    optRemoveAd.className = 'opt-off';
  }
  optRemoveAd.innerHTML = 'Remove top ad';
  optRemoveAd.style.cursor = 'pointer';
  optionsDiv.appendChild(optRemoveAd);
  optionsDiv.appendChild(document.createElement("br"));
  
  // Countdown Interval
  if (countDownInterval) {
    optCountDownInterval.className = 'opt-on';
  } else {
    optCountDownInterval.className = 'opt-off';
  }
  optCountDownInterval.innerHTML = 'Refresh every = ' + countDownInterval + ' seconds';
  optCountDownInterval.style.cursor = 'pointer';
  optionsDiv.appendChild(optCountDownInterval);
  optionsDiv.appendChild(document.createElement("br"));
  
  // Notification Sound
  optNotificationSound.style.fontWeight = 'bold';
  optNotificationSound.className = 'notification-sounds';
  optNotificationSound1.className = 'opt-off';
  optNotificationSound2.className = 'opt-off';
  optNotificationSound3.className = 'opt-off';
  optNotificationSoundOff.className = 'opt-off';
  switch (notificationSound) {
    case 1:
      optNotificationSound1.className = 'opt-on';
      break; 
    case 2:
      optNotificationSound2.className = 'opt-on';
      break;
    case 3:
      optNotificationSound3.className = 'opt-on';
      break;
    case 0:
      optNotificationSoundOff.className = 'opt-on';
      break;
  }
  optNotificationSound.innerHTML = 'Notification Sound';
  optNotificationSound1.innerHTML = 'Fart';
  optNotificationSound2.innerHTML = 'Boing';
  optNotificationSound3.innerHTML = 'Pop';
  optNotificationSoundOff.innerHTML = 'Off';
  optNotificationSound1.style.cursor = 'pointer';
  optNotificationSound2.style.cursor = 'pointer';
  optNotificationSound3.style.cursor = 'pointer';
  optNotificationSoundOff.style.cursor = 'pointer';
  optNotificationSound.appendChild(document.createElement("br"));
  optNotificationSound.appendChild(optNotificationSound1);
  optNotificationSound.appendChild(optNotificationSound2);
  optNotificationSound.appendChild(optNotificationSound3);
  optNotificationSound.appendChild(optNotificationSoundOff);
  optionsDiv.appendChild(optNotificationSound);
  optionsDiv.appendChild(document.createElement("br"));
  
  // Siren Alert Option
  if (sirenSound == 1) {
    optSirenSound.className = 'opt-on';
  } else {
    optSirenSound.className = 'opt-off';
  }
  optSirenSound.innerHTML = 'Siren Headline Audio Alert';
  optSirenSound.style.cursor = 'pointer';
  optionsDiv.appendChild(optSirenSound);
  optionsDiv.appendChild(document.createElement("br"));
  
  return optionsDiv;
}

function getCookie(c_name) {
  c_name = 'dre_' + c_name;
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++)
  {
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name)
    {
      return unescape(y);
    }
  }
}

function createCookie(c_name, dataArray, expireDays) {
  // Creates settings cookie
  var nowPreserve = new Date();
  var oneDay = 24*60*60*1000; // one day in milliseconds
  expireDays = (expireDays || 1);
  var thenPreserve = nowPreserve.getTime() + oneDay * expireDays;
  nowPreserve.setTime(thenPreserve);
  var expireTime = nowPreserve.toUTCString();
  // Define the cookie id and default string
  var cookieId = 'dre_' + c_name;
  var cookieStr = '';
  // Loop over the array
  for(var i=0; i < dataArray.length; i++) {
    cookieStr += dataArray[i] + ',';
  }
  // Remove the last comma from the final string
  cookieStr = cookieStr.substr(0, cookieStr.length - 1);
  // Now add the cookie
  document.cookie = cookieId + '=' + cookieStr + ';expires=' + expireTime + ';domain=' + document.domain;
}

function countDown(){
  if (countdownEnabled != 1) {
    countdownDiv.innerHTML = '00';
    return;
  }
  // Countdown handler
  countDownTime--;
  if (countDownTime <= 0){
    countdownDiv.innerHTML = '00';
    countDownTime = countDownInterval;
    clearTimeout(counter);
    window.location.reload();
    return;
  }
  var countDownString = countDownTime;
  countDownString = countDownString + '';
  while (countDownString.length < 2) countDownString = '0' + countDownString;
  if (countDownTime == 10) {
    countdownDiv.style.color = 'red';
    countdownDiv.style.fontSize = '400%';
    countdownDiv.style.fontWeight = 'bold';
  }
  countdownDiv.innerHTML = countDownString;
  counter = setTimeout(function() {
    countDown();
  }, 1000);
}

function addStyle() {
  // Adds embedded style to head
  var css =
  'a:visited {color:'+linkVisited+';}' +
  'a.unseen {background-color:'+linkUnseenBg+';}' +
  '#dre-menu-container p {margin:0.2em 0;}' +
  '#dre-menu {line-height:1.5em;}' +
  '#dre-menu a {margin-bottom:0.2em;}' +
  '#dre-menu a.opt-off {color:#ccc;}' +
  '#dre-menu a.opt-on {color:#000; font-weight:bold;}' +
  '#dre-menu .notification-sounds a {float:left; margin-left:10px; line-height:1em;}';

  var style = document.createElement('style');
  style.type = 'text/css';
  style.appendChild(document.createTextNode(css));

  document.head.appendChild(style);
}

function loadSound(soundNumber){
  // Loads the appropriate sound data
  var sound = [
  // Sound Off
  null,
  // Sound 1: Fart
  'data:audio/wav;base64,UklGRpQSAABXQVZFZm10IBAAAAABAAEAIlYAACJWAAABAAgAZGF0YXASAACBgYGBgYGBgIGBgYGAgYGBgYGBgYGBgIGAgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYKCg4ODgoODg4ODhIWFhYaIioqJjI6NjI6PjIqIhH+AhImLiYaCgH9+gIWKiYWCgHx2cnFuaWdnZ2NdWFZUUEc6LSIXDAsiP1Vrh52kpamxt7q/xMjLy8nFwLivp6GenZ2foaOjo6KhnpyZl5SRjoyKiIeGhYSDgoKBgIB/fn18e3l3dXRycnN1dnV0dHR0dHV4e3x8fHx8fHx9fXx6fH18e3t8e3h2dG9pZF1ZWWBlaG1taF9VT0lBMSQkJSQmNU9meImapa20vcTIy83Oz83Iw722r6qnpqWlpqenpqSjoZ6bmJWTkI2LiYiHhoWEg4GAfn18enl5eHd2dXRzc3Jyc3RzcnNzc3JzdHR0dHV2d3l7fH1+gIB/fXx8eXd1cW1paGhnaGtvc3RycnJpYFpQRDcsJRsOCRAfKDlOYHKKpLW/x8zR1dnc3NnV0MrEvrm1sK2rq6urqqqpp6ShnpuXlJGPjYuJh4aEg4F/fXt6eHd3dXRzcnFxcHFxcXFxc3R1dXJvbW1sbGtra2tsbXBzdXd6enl5eHd2dnVycHBwcnV5fYCAhIeIhn90aWBURkNAMSYjHxcQFSM0QVJnfJGsxM/S1Njb3N7g39zX0szGwLq1sa2sq6qqqqmnpaKfnJeUkY6LiIaFg4F+fHt5d3V0c3Fwb21sbG1tbnBxcXJzc3BubWxramtsbGtra2xucHBwb29wcXF0eHh3dnZ0cnN2en6Bg4WIiYaCe3NqYVhNQz09NisfFA4RIzI3P1ZzjKXB1NfZ3eHj5ejo5eHb1c7Iwbq0r62rq6uqqaeloZ6alpOQjImHhIJ/fHp4dnRycXBubWxra2tqa2tra2xtbGxsamloaWppamtramlpamtsbGxsbW5vbm5xdHV0dXZ3eX2AgoSIjIyNj4+JgHlvZFtQRD05MSkhFBAPEREYMlmGprrI09vf4+js7e3u7evm39XLwLavq6qqqqurqqmmo5+blpKPi4eEgX58enl3dnRzcW9tbGppaGdnZmVkZGRlZmhpaWlqbG1vcHBwbm1tbGtsbm1rbG1tbm9wcXN1dHFucHBwc3yIk5ibnJqVkIuFf3lybGJXTUlAMyspNDszJxwZITJkm77M0tnd4eXq7e3s6+rp5d7Ux7qwqaalpqenp6akoJyYlJCMiISAfHp4d3Z0cW5saWdmZGNiYWFhYmFiYmRlZmdoaWpra2xub3BxcnFwbm1sbW1tbW5ta2tsbW5xdHZ7fn58e32Djp6qrKurppuPiIN8cWZcUUdAODApKy0xP0dAMykrOlZ9pcfZ3d3h5efp6uno5uXj3tfNwbWspaKgoKGgoJ6cmJWQjIiDfnl2c3Fvbm1qaGZlZGRjYmFgYGBgYWJkZmhqamtsbGxsbnBydHRycXBua2pra2prbGxsbXBydHd8gYaLj5GRkI6Rl5ucmpaPhXtyamJeVkdAODEvMDQ1PEZGPUFJTllymsXe4+Pl5+jq7Ozq5+Pf29XMwbetpaCdnZycmpiVko+MiISAfHh0cW5ta2poZmRjYmFgYGBgYGFiY2RmaGpsbW9wcXJzdHR0c3Jyb21tbm5tbW5vcHFzdXl8gYeMj5OVkY2KiISChomMjYmAeXBlX1dQTEQ6NzY0LiEdIzdNX3CBkqbCzc/f7u7r7vLw7Ojj29HIwLqzraejoJ6cmpmWk5CNiISBfXp3dXJvbWtpZ2VjYmBeXV1dXV5fYGFiY2RlZmlrbW9xcnFxcXFxcnR0c3JzdHNzcnJzc3R2eHp9gIKCg4iOlJmdoKGipKWin56alI2GfnduaGJcVU9HPDxCQDY6PTcwLDM+S1hhZHSZu8XD0eXm4ePp5+Pg3djSy8W9t7Coop+cmJaVk5CNioaBfXl1cm9saWdmZWRiYmNjYmFiYmFgYGBhYWNlZ2lqa2xtb29vcXJ0dHV2eHd2dXV1dnd5fH1/hIaFhYmJh4mMjo+RkpGRlJmZlpKRjomCe3RtaWNXTUU/NzAxMigmLCkoNkdYd6bFxsHO4+nn6e3s5+Ti3dXMwreupqGdm5qYlpSSj4yJhYF8eHVxbmtpZ2dmZWRjY2NiYmJjY2NkZWZmZmdpa2xvcXJzc3R1dnZ3eXx/fn1+f35+gYKCgYGCgoSFh4iJiYuMiYmNj5GYoaKdm5uXj4eEf3ZuZ2BaV1NLSEZANjEuKCYpKSUpNUFZirXBwMzd5Ofs8fHt6+nl4NjOw7espJ+cmpiWlJKQjoqFgXx4dHFuamhnZmVkY2NiYWFhYWBhYWJiZGVnaGlqa2tsbW5vcHN1d3l7fX1+f4GCg4SEg4ODgoGCgoKCg4SFh4mJiouNkZqipKGfnJaNh4N7cWlkX1lRSkVBOjIyOjc0Ny4hIi00P2eivbrA1+Pi5e3w7+3t7Ojg1sq9saaempeUk5KQjYqIhIB8eHVxbmtpZ2dmZWNiYWFhYWFiY2NkZmhoaWlrbGxtb3FzdHd5e3x9f4GCg4OFhYWFhoaFhYWDgYCAgYSFhISFg4KEhomQnKSjn5+ckoeEgHduaWNZT0pHRkA6NztBPjU0NjU0PkNEYZq8vsLZ5ubo8fb08e/t6ODYzcG1qaCalZKQjouIhYN/e3ZzcG1qZ2RiYWFhYWFhYmNkZGVmZ2hpa2xtb3BxcXJ0dnd4eXp7fH1+gIGAgICBgoOEg4OEhYSDhIaHiYqJh4WEhISFhYiSm5mRjo6FenRxamNfWlRPST45PUJEREI9NTlIVVtebZS4x8va6uzq7PLy7uvo4trRyL6zqZ+Yko6Lh4WBfnp2cW1oZGFdW1hWVlZXV1hYWFlbXF5gYmNlaGttcHJ0dXd5e3x+gYOEhYeIiIeHhoWFhYWGiImIiIiHhYaIiIaFgoCAgYSGhoaLkpOQjoyFfXhzbGVgWldVUk9KSEpMT1FMR0ZGTVtkYWJ9n7C71OTj4OXr7Ozs6uTd1s7FurClmpGLhoOAfHl1cWxoY15aVlRSUU9OTU1OT1BQUVNXWl1gY2ZqbXF0d3p8gIKEh4qMjY+RkZKTlJSVlZWVlZWWl5iYmJeVlJSTkI2LiIeHhH57fHx6d3VzbmZhX11ZV1ZVVFNTU1RVWFpdZGRbVlxnampoa3qHncrn4tvi6ejo6+vl3djRycG3qp2Rh395dnNwbWlmYl5aVlNRUE5LSkpLTE5PUVNXW19jZmtvcnV5fICDhomLjZCSk5SVl5iYmZmamZmZmpqampqZmZiWlJORjYiEf3h0c3BrZWJgYGBlbW1paGlmZGZnZWVkZGRjY2NjY2RgW1lgZ2ppaHJ+iaPP4dnX3+Le4OLf2NLLw7uzqZyPhHtzbWpoZWJgXVpYVVNSUVFRUVFRUVNXW15hZGdrbnF1eHt/g4aJi42PkZKUlZaXmJiZmZqbmpqZmJeWlZOSkY6MiYWBfXl2dHBra2tpZWNjZGVncXd1dHZ1cnFzcXBwcW9samloZ2dpaGNdXmRqbWpufIGMstba0tfd29jb29XNx8C3r6aajoJ4cGpmZGNhX15cW1pYV1ZWVldYWVtcX2JmaWxvc3d6foKFiIuNkJKTlJWXl5iYmZqbmpqZmJeVlJKRkI2KiIaEgX16dnNxb29taWZmZmdnZ2ltcHZ/hIODhIOAgIGAfnx6dnNxbmppaWdlZF9eYmltbW9zeHWCt9nQyNHVzc3Qzsa/ubCpo5yRhXtya2VjY2JhYF9fXl1cXF1dXl9gYWNmam5xc3d7f4KFiYyOkZOVl5iZmZiYl5eWlpaUkY+MioiGhYOBfnt5eHZ0cnFubGpoZ2ZlZGVmaWprbW9wcnyJi4iKi4eEhISAfHl4dXBpYmFnbXF2eHZ8gHuCosHBvMLEvrq8ubKsp6Gbl5GJgHdvaWRiYWFhYWFhY2RkZWZoaWttb3FzdXh7fYGEhoiKjY+QkZOVlZWVlZWUkpGPjYqIhYOCf316eXZ0cnJycXFxcnJxcXBvbWtqbG5wcXJzdHV2eH+Hi4qKiYaCgHt3cGdmZmdtdX6EfHiUub64vsK9uby6tK6onpWOiYF4cmxnY2JiY2NkZGVnaGlqbW9wc3Z5e32BhIeJi42PkJCRkZGPjo2NjIyMi4qKiYeFhIOCgH9+fXt6eXh3d3d3eHl5eXl5d3VzcXBtbG5vcHN2eHh2eYOIhYWFgXhsZmprc4KEfHySpZ+dpq6qqq+vq6elnpeQi4N8d3NvbWtra2trbG5wcXJzdXd5e36Bg4WHiYuMjI6Ojo2NjIuKiYiGhIOBgH5+fXx7enl4eHd3dnZ2dnZ4eXp7fH6AgoOEhYWFhYSCfnp5eHZ4end0dXd2c3V3dXJvbGdkZG1+iIqNkZWfpaSjo6GenZuYk42HgHp1cW9tbGxtbnBzdnh5e31/gYKEhoeJi42Njo6Ojo6OjYyKiYiHhYSCgH58enh3d3Z1dXV1dnZ3d3h5ent9fn+AgIGBgH59e3l4d3Z0c3NycXF0d3l8gYSFjJSYl5iXkoyJhX59fXl0cHR7foSGhIKDhIeIiImIhoSDgYB+fXt6ent8fYCBgoSFhYaGh4eIiIiIiYmKiYmJiYiIiIeGhoWEg4KBgYB/fn18e3t7e3t7e3t7e3t7e3x8fX1+gIGBgIGDgoCAgH17enl2dHRybWljXV9sfYqSnp+coKWkoqKgm5eUkYyHgnt1cGxqaWpsbW9xc3Z4e32AgoSGiImLjI2NjY2NjIuLiomHhoSCgX99fHt6eXh4eXl7fH2AgYKEhYaHh4iJiYmIiIiGhYOCgH59e3p5d3Z1c3FwcnZ6fYCDg4OFhoaHh4aFg4GAfn19fX5+f4CAgYKCgoKCgoOCgoOFhIODgoKBgH9+fX19fXx7enl4d3NvbWppcIGSkY2Qk5OSk5KQjYuJhoSBfHdzb21sbW9xc3Z5fYCChIaIiouMjY2NjY6OjYyKiYeFhIKBf317enh3dnV1dXV1dnh5e31+gIGCg4SFhoeHiIiIh4aFhIOBgH9+fHx8e3t6eXl5eXt/hIiIiYuLioqKiYiHhYSCgH99fHx8e3p6enl4eXl5ent8fX18foCCg4SEhIaHiIiIiIeGhoWFhYSEhISEhISEhISDg4ODg4KCgoKDg4ODg4ODg4OCgoKCgYGAgICAgH9+fn59fX5/gH59fX59fX5+fn5+f358end3gImIhIeKiYmLioeGhoWDg4KAfn18enp6e3x8foCBgoOEhIWEhISEg4KCgYGAgH9/fn5+fX19fX1+fn+AgIGBgYGCgoKDg4ODg4ODg4ODg4ODgoKCgoKCgYCAgICAgIB/f35+fX18fH6BgoKCg4OCgoKBgYCAgH9+fn19fX19fX5/gICBgYKCg4SDg4KCgoKCgYGBgYGBgICAgH9/f39/f4CAgICBgYGBgYGBgYCAgH9/f35+fn59fXx8e3t6enl4eXh5fH+Bg4WFhYWFhIOEhIODg4ODgoKBgICAgICAgYGBgYGBgYGBgoKDg4SEhISEhISEhISDgoKCgYGAgICAgH9/fn5+fn19fX5/f3+AgICBgYGBgYCBgoOEhISEg4KBgICAf39+fX19fX5/gICBgYGCgoKCgoKCgoKCgoKCgYGAgICAgIB/gICAgYGCgoKDg4ODg4ODg4ODg4ODg4KCgoKCgoKCgoKCg4ODg4ODg4ODg4ODg4KCgoKCgoGBgYGAgYCAgIB/f35+fn5+fn5+fn5+f3+AgIGBgYGBgYGAgICAgICAgH9/fn18fHx9foCAgICBgYKCgoKCgoKDg4KCgYGBgYGBgYGBgICAgH9/f35+fn19fX5+fn9/gICBgYGBgYGBgYGBgYGBgYGBgYGBgYGCgoKBgYGBgYGCgYKBgYGBgYGBgYGBgYCAgICAgICAf39+fn5+fn19fX19fX1+fn5+fn5+fn9/f4CAgIGBgYGBgYGBgYGBgYGBgYGCgoKCgoKCgoKCgoKBgYGBgYGBgYGAgICAgICAgICAgICAgICAgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGCgoKCgoKCgoKCgoKCgoKCgoKCgoGBgICAgICAgICAgICAgICAgICAgICAgICBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgoKCgoKCgoKCgYGBgYGBgYGBgYGBgYCAgYCBgYGBgYGBgYGBgoKCgYKCgoKCgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGAgYCA',
  // Sound 2: Boing
  'data:audio/wav;base64,UklGRiQPAABXQVZFZm10IBAAAAABAAEAESsAABErAAABAAgAZGF0YQAPAADONCoqbWt4a840KiqAgIGKioqKjIyMjIqCioqBf352dnFqZ2VqbnFxcWdecXZ+gJ+fkn9eTXSMmamwqaq0mYFqSUBWZ3SAgoqWloF0V0RAV3F/go+WoqKWgGpeXm6AipKWn6eWgnRhVlBnfn+Ct8GCWS9AYXaBjKeqn4FlTT88WX+WqrTBxLeWdFY2P152go+Wn6eSdl5ANUBZdIGPn6qnkoF0YWFlcYGbqrS3qpl0TGFudoqwqpZuNioqTH6WqrfQwZt2QCoqKkx/m7fV1dW3gVAvKjBhiqe80dHBonRAKioqNV5/mbCzqY90VjwwP1l/s9XVp3RZXn+PosrKs5ZqTDYrNVZ0j7O8t6mCXj8rKj9Zf5+8wcS3ln9lTU1ZboGfqamfgnReST9JUGeAj5+nn49/WTZMdIrA1dXAik0qKjxuosvV1dXEkk0qKioqRH6p0dXVtIFMKioqK1aCvNXV1cqZbkAqKjZZganK1dG8lnFEKyorQIHKt4JNPEBXfqLRy8GfbkkwKjZhgbTV1dXRp3RJKiovSXSbwMrKqoFhPCorPFeBp7zKxKqMcVBATFdxjKK3wbypgU0qK01+qsHBqnY8KioqUH+ny9XVxJlnPCoqL1CAs9XV1cqfflA1LzZQdI+ptLSfgWFALys1SWWAm7O3qpuCdmpnanab0cGbalBWZX+ZwMG3mWc/KioqP2GPvNHVyqd+UC8qMEx2osTV1cSif1A1MD9Xf5mzwcCpimdANTU/V36Pqbe0qopWMDxeirfKxKlxPyovTX+p0NXV0ap+QCoqKi9WgKnEyreWcU01L0BhganK0dHAn39eSURNZX+Pn6eijH5hTERJUGF+lreqgFY/QFB0mcTQ0LePbkw1Nk1ukrzR1dCwgWE1Kis/YYGftLypjG5ELys8UHaPqbe8sJZ/ZVlZZ3+Sqbe8s6KAQCo1WYyzt7CBRCoqKkB/s9XV1dWwcTUqKipQgbTV1dXBilYqKiovV4K00NHBm24/KiorTHafxNXVwZl2UDw/UGqKqsvVvIpZNjA8V3+itLyqimdEKys2UHSSsLy8p4JqUElNZYCbs8HBtJ+BaldWWWp+gYyPgn9qV0xNVmV2gY+WlpKKf15NZ4zB1dXEjEwqKipNisrV1dXLjEAqKioqVozK1dXQn2UvKiowYZLE1dXVqn5JKyo8XoGnwMSzknFJMC88V36Poqqzp3ZJKy9MdpvQ1dXQom5AKzBQf6fK1cuzgU0qKio2WYCitLCZflA8NURngrDL1cqqgmdQTFZxjKnAwbCPbkxARFBWVnSfy9XLllAqKioqZanV1dXVp1kqKiovZ6fV1dXLgkQqKipJf7TV1dCiajAqKjVhjLfQy7CBUDUvQGeMt9HVxJ90V1lNRElhgKK3xLyPXi8qKjVZirzV1cCPZ0k8TW6Ms8rEs4xqVk1QZ4CSp6ePflY/PEBXdo+fopJ/allZZ36Mqbeif1lulsDQwI9XKiovWZbR1dXEikkqKipAdqfKy7CATCsrP2WSxNXVwY9hQDxQdpnA0MSidkQqKjxegJuwqpJxUERMYYCis6mPf25qdoqbn5mKf25lZW5/gpKPgXZlV1dhbn+CjIp/dnF0f4GPmZuWjIB2dn6Aio+SkoJ/cWdncXZ/gYGBcUw2VoLE1bR0KyoqPHbK1dXVlkQqKkSBytXVy4I/Kio2carV0ap0NiorSX6pxMGfcUw/TW6PtMS3kmpNTGGAore0m39lWVBNWXGCmZuWgWdQTV5/lqmpkn9qZ3SBmaenkn9qZW5/ipKMgHRhXmd0gIGBfmphYWd0f4GKgoB/f4CKlpubln9ZTHGz1dWwVyoqKlCp1dWzWSoqMHG81dGPRCoqUIzL1cSCVjxMdKfQ0bB/TURXfpmnlnRNP0llgZubgWFMTGF/lqKbkoFuWWF+m7Owm39lYXGKp6qZf1lQYX+Sm4x2YVdhcYCBgHRqanaBkpmPgX9+f4KPj4yBf35+fn92cWdlZ252f39xV0xlotXVjD8qMGrB1dWZTDVNj9XVqmU2P2eZs5tqPz9XgJ+ZflZMWX+SmYp+dH+KmZmMf3Z/gY+WjIF/f3+BgX92dIGCalZXdpKZgm5MTWWBm5uBdHF/j5mSgXZ2gYyZloJ/fnZ+dnFqZ2p0fn9+dnZ2dnZ2dn5/gYGCgoKBgYGCgoKCf25lgbzEijwqNn+zqn5MTG6Wp4xuXnGKmY+Adn+KkoyAf3+CjIKBf4CBgX90cXF0dnRuam50dHFubnF2f39/gYKCgpKqj15XgcHKlnRugJmMdmVhdH50amVudnZxamp0fn5/f4GMjIqBgYKKgoGBgYKKgYB/fnZ0cW5xdn5+fn5/f4CAdmVhgsTAdkBXmbySamqCmYF0doqbjHZqdoF/bmVqfn92cXR/gH9xcX6AgYGBgoqKgYGCgoqCgoqKioF/f352dG5ucXSBglA2ZaKnZ0x0lo9xboynj3Z2ipuKfn6Bj4J/f4CAfnFudHR0bm5xdHR2dHZ+fn9/gYGBgYGBgYGBgYGBgoqMioB0fqewbjxWjIxhWYGWflZhgIFuYXSKjIGBio+CgICBgoGBgYKBgH92dHFxcXF0dHZ+fn9/fn9/f39/gYKMj4+PjIybn2VMgbeWWWqbjFdWfoFnUGV/dmpxfn9+dn9/gIGCioqCgoGBgYGCgoqKjIyKgoF/dnZ2dHZ2dn5+dnZ2dHR2dnRuf6mqcVmBooBngaKKdH+Pj4B+gIB+dHZ+dnR0dnR0dnR0dHZ/f4CCjIyMjIqCgYGBgIGBgYKBgH92cW5ubnFxdHSBlmE2drCBTXGpjGV/n5Z/f4KKgICBgYGBgYB+dn52cXF0dnR0dnZ2fn5+fn5/gIGBgoqMjIqKioyKgoKCgoKBgH5xan+ScUxngXRXaoB2ZWp+fn6BioyKjIyKgYGBgYGCgoKBgYF/dnR0dHZ2dn5/fn5+f39/f39/gIGBgYGBgYGBgH+BmYBEarOSV3GZf1Bugn5udn92dn52cXZ+fn+AgYyPj4yMioqBgYGBgoqKgoGAf3ZxcXFxcW5ucXFxcXFxcXZ+f4CAgZKpj3aCmYJ/gpaCgIGBf35+dnR0dnZ0dnZ2fn9+dnZ+f39/gIGCgoKBgYKCgYGBgYGBgX9/dnRxcXFxdn5+dnZ+gYFJTZmWZX+njHaKj4GAgX9/gYGBgoqBf392dHR0dHR2dHR0dHZ+fn9/gIGCioqMjIqCgYGBgYB/f4CAf352dG5nZX+KbmF/f2pxgH9udHZ+gYyMj5KSj4qKgYGCgoGBgYGBf3Z2dnZ2dHR2dnZ0dHR0cXFudHZ/gICBgYGBgYGBgYKMmZJndKmKYX+Cbmd+dnF2fnR0fnZ2fn9/gYKMjIqKgoKCgYGBgYGBgYF/f35xbmpqcXF0dHR2dnZ0dHR+f3+BjI+Pioqbn39/in92gYKAgIB/fn5+dHZ+fn5+dnZ2dnRxcXZ+f4CBgoqCgYGBgYGBgYKCioKAf3Z0cW5ubnF0dHR0dn52f4BhVo+SboqfgoGMgoKMgYGBgYGBgYB/fnZ2dHR0dnZ2dnZ2fnZ2dn5/gYGBgoKBgYB/f39/f3+AgICAf39+fnZ2dHaKinF+gXFxf3Z2f4CBjI+MjIyKgoKBgYKCgoKCgX92cXFubm5qbm5xcXR0dHR0dHZ/gIGKioyMj4+MioKCgoGBkpJlZ5l2V4F/ZXR2dH5+dHR0dHZ2f4GCjIyKgoGBf39/f3+AgYGBgH9/fnR0dnZ2fn5+f39+dnR2fn9/gIGCgoGBgYGWjH6Bgn+AgX9/f3Z2fnZ+fn5+dn5/fn52dHF2fn+AgYKKioKBgYGBgICAgYGBgH9+dHFubm5xdnZ+fn5+fnZ2f4yBZYyqf4KigYCBf4GCgYGBgYGAf392dHZ0dHR0dHRxcXR0dHR2fn+AgYGBgoKCgYGBgYGBgYGBgYF/f39/f352dHR/gn9xf35udHd3f4GBi46Li4mBgX9/f39/f39/d3Vyb29vcnV1dXd/f39/f39/gIGBiYuJgYCAgIGBgICBgYGAf4F/XHOLbGyBd3V/d39/d3V1d39/f3+AgYGBgYGAgH9/f3+BgYGAf39/f3d3d3d3f3d3d3Z2dnZ3f3+BiIqKiIGAf3+Kin+AioCAgH94eHh4eHh/f39/eHh4f39/f39/f4CBgYCAf39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f4eAdYuYf4GNf3+Af3+Af4B/f395eXd1dXd3d3d3d3d1dXV1f3+Ah4mNioiBgYGBgYGAgYGBgYB/f3l5d3d5eX95eXl/gHZ5f3l/f39/gICAgICAgICAf39/f39/f395eXl5eX9/f39/f39/f395f39/gIGBgYGAgH9/f39/gICBgYGAgH9tf4Z1d396d3p5eXp5eXp/f3+AhYGAgIB/f4CAf3+AgH9/f39/enp6en9/f39/f39/f39/f3+AgYGFhYWBgICAf4CFgH+AgH9/f3l5eHh5e39/f39/f397enp/f3+AgIGAgIB/f39/f4CAgIGBgH9/f39/f39/f39/f397e39/f3+AhICAgH9/f39/f39/f39/f39/f3t7e3t7f39/f39/f39/f39/f4CDhYWFg4CAf39/f39/f39/f39/f39/f39/f39/f398e3p6fH9/gICAgH9/f39/f39/f4CAgH9/f39/fHx/f39/f39/f39/f39/f4CAgICAgIB/f39/f39/f39/f399fHx8fH19fX9/f39/f31/f39/gIKAf39/f39/f39/f39/f39/f399f39/f39/f39/f39/f39/f3+AgH9/f39/f39/f39/f39/f319fX18fX19fX9/f39/f39/f39/gYGBf39/f39/f39/f39/f39/f35+fX1+f39/f39/f35+fX9/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f38=',
  // Sound 3: Pop
  'data:audio/wav;base64,UklGRmQLAABXQVZFZm10IBAAAAABAAEAESsAABErAAABAAgAZGF0YT8LAACAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgH9/gYB/gX9+f4GDf36BgoF9fYKCf4CEgHx/gYKAfICEgoB8gYJ+gH98f4CChIWDfXt8foGChoCEhHt7e3+GgHyBe359gICAgn+Dfp7XmhcubHuEiZ/Fu5JgMDVVlLzYx41AISdosePYmloqJlun3NqfWSQqZK7bzpJOJTx5u9W4e0QtUJHHy51fN0N3rMKufE9DXo20uZdmR1F6pbSfd1hWb5KoooViVGWKp6uRbFhfeJalmn1iXG+Mn56IbmFpf5Waj3hlZ3yTnZF4ZWNzjZ+ahWxfaYOano91ZGd5jZaQgXBrdoiSjoBybXaHko+CdW50g4+QhXVucoGQk4l8cm52g4uNiH11doCJiYN7dXV8hoyMhXx1dX2Fh4aCfX1/gYODgH58fYGFhIB7en+EhoOAfXt6f4SHhX96e36Ag4SCgH17fIGGhX98fn59foCBg4SDf3x7e36Eh4WDfXh4fIKIiIR+eXh9gYSFgn58fYKFhH96en6ChoeDfXl4foWIhoF7eHuBhoeDfnl5foSIhoF7eX2CgoOCf318f4KFhH97en2ChYSCf3x6fIGFhoOAfXp6fYGFhoR/fHp6fYKGhoSAenh6f4OGhYJ9fH19f4GCgYGBgYCAfnx+gYKCgoF/fX2Ag4SCf3x7f4SGg398eXyBhYeFgHx5en6Dh4eCfHl6foSGhX97e32BhIWDgHx6e4CEhYOBfnx9gIKDgX59fn+Cg4KAgH9+f4CAgIB/gYGAf39/gIGBgICAf39/gICAgIGAgICAgH9/gIGBgH9/f3+AgYKBf35+gIKCgYB/fn+AgYGBgICAgH+AgICAgICBgYB+fn+AgoOBf319f4GDgoGAfn1+gIGCgYB/fn+AgYKAgH9/gICBgYB/f39/gIGBgYGAfn5/gYKCgX9+fn+BgYGBgX9+fn+BgoKBgH5+foCCgoGAf39/gICBgYCAgH+AgYGBf35+gIGCgoB/fn6AgYKCgX9+fn+BgoKBf35/gIGBgX9/f4CBgYCAf3+AgYGBgH5+f4CCgoF/f39/gIGBgH9/f4CBgYB/f4CAgYGAf39/gIGBgYB/fn+AgoKBf35/f4GBgYCAf3+AgIGBgIB/f4CAgYGAf4B/gIGBgH9/f3+BgYGBgH9+f4CAgYGAf39/gICBgYB/f3+BgYGAgICAgICAgICAgICAgICAgH9/gIGBgH9/f4CBgYGAf39/gIGBgIB/gICAgICAgICAgIGAgH+AgICAgICAgICAgICAgICAgICBgYCAgICAgICAgICAgICAgIGAf3+AgIGBgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGAgICAgICBgYCAf4CAgIGBgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIB/f4CBgYGAgIB/f4CAgYCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYCAf3+AgIGBgYCAf4CAgIGBgIB/gICAgYCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgAA=',
  // Sound 4: Siren
  'data:audio/wav;base64,UklGRgQZAABXQVZFZm10IBAAAAABAAEAQB8AAEAfAAABAAgAZGF0Yd8YAACAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgYCAgICAgICAgICAgICAgICAgICAgICAgICAgIB/gIB/hJmoqZ+TgWlXUldlepSlq6aahGpSSFFpgp2usKiYgm1dVFdqfo2XnZuViX1zb29yd3yBh4yPjYiAeHFucXiCi5GSj4d9dG5ucnuEjZGRjIR7c25uc3qCiY6Pi4aCfnp2d3p7e3yBiIuOjImFeW5rcXt/h5ujnY6Bd2heYnGBhYmXnJmLgH1yY2FrfISJlZiYkYR/dmdgY3OCh5Sdm5WEeXZsZGNvgIaMlpqajH16cmVfZniGipKYm5eGeXhtY2FvgY+Sl5mUiHlyc25pbXeIkI6Pk5CGfXh5cm51gImKi4yNi4V+eX54cnF1fYKBg4mLh4F6fHx3cnZ9gn+Ah4uLhX99fnhxcHeCg4WHiYiEg4OHhX13eHl7fH6HjI+QjIWFfnJucHR7f4KJjpKUj4aEeGlmaHJ/iI2UlpKLfXNzcGxtcXqEiYuQlJKMf3VzcmxwdHyHiYmNjYmEfHV0dXZ5fIKJiYmKiIWFhIJ/f4B7enl7gYaJh4eEhIWBe4CAfHt5eICDhoeFhYSEgHt+eHFycnJ9goiIhYCAhH53eX5/gXt5f4KHiYSDh4qEfnx9e3t6fIKHiIqHgHyDhYF/foKGg3x8gYGDhoR7e4OIh4B5e397dnl/fYCGioiEf3+Ifm1ncXuAhYyUk4+LhnVhX2t2e4WVn56aiHFgWFppgZSgp6SVfGNYXGZ8lKKlnpB7aV5gbICSnpqOgXZubnSBiYyQh3p2en+Gjoh/eW9re4GHlJCAd3Fpb3uJlZCHe3N3fIKMioJ+fHuChIqKf3dzdoSNkYl8dXB0goqUkHx0cnWHj5GGc2hpeo2Wl4Z0amRyiZKThnJrdYiWmYx1aGx9jZmUiHVvfIWRjXxtZ3GDkpaFcGduhJCRgWthaYWdo4tvXmN/mqKNb1tjhaGpk3FcY4Gfqpd3YmqJpKmTc19oh52dhWphbomXkXllZnSMlolwY2yDl5R+aWp7j5eHcWVzjJyUeGJngpyii21dcJOsoX5cXn+hqo1qVWiSqZp2V1l/pKODX1BtnayVb1RgjKifgGRgg6ajhmZbcZyjjm9ebJSmkW5bZY2ml3JbYYalm3ZYXIGlooBfWXmgoIJhWnmkqYtnW3SeqpFqWm6YqpZwXXGYqZZvWm2WppNvXG2WpI9sW2ySnoxsW22SmopvX3GUmYZsX3CRmIRtZXyZnIVtZn+YlX9ra4eemIBtcIybjnNlcI+cjXVqd5GXgmxne5WXgWxqgZaTfmxuhpSMdmtzi5SGcmx8kpaAbGuAlpN7aW+JloxzZ3aRmIRsZXuTl4NsbIGTjnlrcomUhnJugJOVgG1xhpeQem13j5qOeGt6jpWEb22Bk5J/b3aJkIRwaXuQk4JucIOPiXZqc4eOgnFwgpKMeGdxh5OIdG18kpWGc3aGkol3bHuRlohzbYCRjHlpcoaTiHZsf5GSgG1ugpKNe215jJWLdW57iYqCcneFjId4dX+LiH9xeISMh3t3gYmFfXB3gomCeHN6goF8dnyDhYB5eYOJg313gYuMg3t6ho+IfnaAiI2EeHeGjouBdXuHjIV8eICKiYB2e4CFfnV1fYB+d3B7g4N7dnqFioB3dIKLi353f4qNg3d1hI2Mf3R7h42GfHiFjYuBd32GioJ6eoOJhHt0foSGfHR1gISBe3eBh4V8dn6HiYB4d4WMhnx1f4iIfHV3hIuFe3aCiIZ9d36IiX50coKKh3tyeYKGgHh5hIeCeHOBiIiBeX2GioWBfoaGgn17hYiHf3qAiIeAenyIiIB3dIKLiH11eIaHgXp3g4WCe3d+hYN9eHqHh4J7eIKIhn13fIeHgXt3g4eFfXh/hYV+eHiDhYN8dn+Fhn95e4WHg3x2gIWFfnl/iYyEenaCioqCeHyFioR/e4SGg3p1foiJfXNygIiGenJ4hIh/d3N/h4R5cniGjIJ2b3uIjIN5eISOiX93foiOhHp4hZCNgnd7iJCHeXF8jI+Dc3ODkYx7a3OHk4l1bHuPlIJqaX6TkntnbYaVinBjdJGZhWpngJmVe2Rri6CTdmVykJyKbmV6l5qBaWiDm5Z6ZmyKnZBzZXOSoI5yZnaSm4VtZ32XmX9oZn+ZlXtmaYSblHhka4qek3ZibIqbjXFicpGbiG1kdpWchmxmepeagWlnf52bgWtpf5yagWlmfZuagWxne5mbhG5oe5ueh25jdZifiW1fcZehi2pZa5SkkW9aaJGmlXNcZ4+omXddY4agl3hfYoOfmn1iYHyam4FnYXmcoYlqXnKXo5BvXWySppZyWmOJqKB8XV1+pKiIZ110nK2Wcl5ni6ide11beKGkh2RVaZankGxUX4qonHpcVnqhpYpoVGmUqZp4W12FpqKEZFl3nqqUcVpnj6qjhWVdeJulk3Vhaoqhm4JlXHWWo5JyWF+Cn5+GZVdtj6OXel1efZqhjW1YaYmgmX9hXXiWo5V3XWSAm6ORb1xriaOjimhfcpCmm3teYnqWopFxXGV/mqGNbF5qiqKihmVecJCkm3xiYneSoJR3YmR6k5+PdGNle5OdjHVlaHySm4t1Z2uBl6CPdmZofpWfjnVmZn+VnYt2Z2l/kpqLeGxtgJGdj3xraXqOnZJ/a2d4jpuRfGlkdIuXkoFxZ3GGk5WFdmdtf5CYi3lmZnmNnZN9ZmJ0jaCYgmtkdI6fm4lyZ3CElJqOd2doeo6dlH1lXm2JnJuGamFsiJugjW5gZoCWoJR4ZWN3j5yZgGlhbYeZno1yY2iBlp+UemVjdY2ZloJuZ2x/jZGKeW5sdoeRkYh4bW58i5OOhXVwdYKQkox/cG15ipSPhXhyeYeSkImBenl7hIqIiYJ5dHR7gIWKgXVvbnuHjo59cm92hoqNiHp3dniBg4qJfnpzdYGFkIl9e3R5goeRi4J+dHeChpKJgnhwdH2GkouCeG50fomSi4R5cXZ+h5OOhXptb3mGko+CdmxwfYmUkYV4bW56hZGRhXdubXeCjZGHe29sdYKOlIt9dHF5g46TkYV5cW97iZOVi3pwcHqHj5SOgHlzd4GJj46DeHFxe4OJjYN4cG50f4aLin51bGt6hY2SiXt0cHaCiI+QhX12c32Hi4yIf3p3eoGDhoiDfXh4eoOFg4Z/fHp2fIF/goeAfX96fYF8g4mEhIJ3eX17iZKLhn9zeXx9jZCKhXptcXV9kZWMgnNncHh/j5GJgHRob3R9kJWNhHZpcHeBlJmQhXdrdHmBk5qTin1ucXeAkJmQiH9zc3V3g5CQjoV0amxwfpKVj4NzZmdvfYyXkoV4amlzfIeUlIyCdm1xeYKRmpKIfGxpcnqLnZiKgW5kbXWBmZ2QhnNjaXV+kJuTioBtaHB2gZOZkIp9bGxyeIaUlY6HeW1vc3qLlpSQhnJpbG13i5ORjoBtZ2ptfI2Sko18bWprcIKSmpqRfXBvbnSGkpiZlH9xb2twh5eeoJZ9cG5tdIaVmpySempkYWl9jJGUi3hrZWNqe4mRmJKCdGxkZ3iLlp+ZiHdtZWl4h5KcmIp9dGxueIaSmJaLgHRsbnWBjZKTjIF1amRncoOSmZmNfW1iY25/j5icl4x8bWZqc4SSmpyUhXZraWt2hZCYmZGCcWVkaHaIlp2YjHtpYWNsfI2ZmpSKeGhmaHKCkZeamIt2amZodIeWnaGbiXRnZGh1h5WZm5aFdGhiYWx9jZWamI17amFgZXiPmZ+hknloYGFqf5SeoaKVf21lYmV1ipegopmFbF5dY2+FlJygnYtzXlVbaX+VoaCdlX5nXV5jdYqaoqKfj3diW15nfpWjo6CYgmxfXWFqgJWfoqKTd19WVFtxjJ2kpZ2Jcl9YXWR2jp+mqaWNcltRV2Z/mqyyq5+DaFRRXm6Gna2vpZl+Y1BOW22FnayqoJJ5YVJQWmyAlKWqo5eDalRMVGmAm6uwp5SAaVZPW3WFl6aqpJaJdWNWWGx+jJynppeMfmlWUV5zg5OfpZ+ThXNhVVlrf4yZpKKXjoBtX1hidIWQmaCZjoV6bWFeZHiJkpmdmpGCd2paWGZ6h4+bn5iNfnRrY2Frgo2OmJ+Zi317dWxpb36Hho2Xm5aHf3dqYmd3hoqRlpeUiH55b2Zjbn6Dh46Skol7eHNqZml2hIuTmZiUhHx6b2RkcIGKi5GVl45/dXJsZWx7jZKQj5CLgXh2d3Fscn2Ji4eJjouCenV5dnV4gYuOi4mJiIN8eYB+eHZ8g4SDg4eGhH97gYF8dnZ9goKChoeGgnx7fXhyc3l/gYGBhISGiIaHg3t2dnl/g4SHioyMiYWFf3Vxcnd8fYKLk5iVioGAdmtpa3mIi4uMjYuHfXd3c2xqb3yDhYaMjoyFenV4c3B1eYGJiYiNi4aAeXZ4fnp3e4OKh4SEhoWFgn9/goN9fH1/g4WGhoaIhoaCfH59d3Z4en+Cg4GChYSGg3x8eHJ1dnaAhYiIhICChn95eHt8fXp6f4SHh4SChoiDfn5/gH97en6GhoiIgH2Dh4N/fX+CgHt7foB/hYR+f4SHhYJ8fn98eXl+fn2CiIiHhICFgHBma3V+gYWNk5ONhHVlYWt2eoSXoqCbinVkXmBpe5Cgp6aXf2VdXmR0jKCnoJB9a2RjbH2Rn52Wg3RsbHJ/i4qKgnZyeXyFjoh+enJseH2FlZWEe3RpbnuEjY2HgHh1eHyFhH5+fH6KiIeKgHNxeISQlo6AdGxygIeRj4F5cnaEjJCLeW1ve42Znox7b2dzgo6ShHRtcoOTl4lxYmV4i5qVhnFreISPjX9vanaIl5eCb2Vwi5WWgm1mcY6goYltYGiEnqSNb1xjgqCljnBdY36apJBzXWKAnaWQcF1mhZ2chWtkdI+bkHdkaHyWm4xzZnCClJKBbW5+j5OHcmh2i5mQeGFkfZach21gcpKjlnpdX4GfpI1rXHKWqJh2XGKKpaCDZF57n6aPbVxqkqKTdmJihJ+Xe2Vje5yZfWZgc5mfhWZbbpWljWhbbZSrmHBbaJGtnnhdY4iqoYBiYoSnn3tbWn6mpYJeWHefooRiXHmeo4dmXXmdooVkWXado4hmWnOcoYZmXnigooNkYH6jpIRkX36fm39kY4WimX1naImej3Fia4+hj3JlcZKbhWphdZadhG1pgZ2aemJlhaOefmdqh56TdWZykJ6KbGJ3l56CZWF8mpt9ZWiFnJN1YmyLmYhuY3eVmYFmYnyYl35qbomajnVpeJKbh29sg5iVe2VrhpmPc2Z1kJqIbmd9k5Z/a2+HmJB7bXeMkoJvboKUkHtsc4uVhXBpfpaYgW1uhpqPd2RwiJWEbml8kpB8aG2EkodyaHyUmoVvbYSVj3hmdI2ainRrgZaXg2twhJWOfXCBk5eIcW5+jop/b3eDioBycH2IgnZqd4WNg3dzgIuFe3J8iY+Cd3eCioV7c3+JjIJ6eoeNhXt2gouNf3d6i5CGdm17io9/c3GCjYh6cHqFjIF4d4SMh311f4eKf3l7homAeHSAh4d8d32Ji392c4OJhXhzfYqKfHBugYqHeG96iIx/dHSGjol8cn6KjoR7e4eLiIB6hYqJf3h8h4uEfnuGiIR6dH+HiH51dIKFgHd0gIiHe3F2g4iBeHSAhYZ9eICIiX93eIeLh3x1foWIgXyAioqAd3SBhod+d36EhX56e4WDfXd6iImEeHZ/iYZ+eX6Ni4J2dYSMin94fYqKg3p5hoiDeneAiIN5dHqJh310dIOHgXRxfIqHenF0iIuEeHOAjIp9eHyNjYV6doaOinx4gJCQhHh2iI+MfXV7iYqAd3aFioV3cHmFiH52dIGFgXVyeYWIfXNygoqHeHF3iI6EdnGAj5GBdXaHlIx8cXyNlIZ3cYOUk4FvcIORiXpteIyShHNugJGMemduhpaMdml2jJKDbGp+k5F+a3OLmYxzZ3iTm4huaH6Xln9oaYSZkXhmcI+biW1gdJadhmtlgZ6afmZoh5+WeWZxkKCOcWV0lJ2GamR7mp+EaWaAnp1+ZWSBm5V2YGSEnJJxXWaJnpFvX2qPoo9uYXCWp5FvYnKWoopqYHKYoohqYHSZoIdrYnWZoYltY3WWnYhtYnKVn4lrXm2Too5vXmyTpJJxXmqTqZt3YGmPqJl2XmWLqJl2XWOHppl3XmCBoZh5YGB+npt/Y155nJ+HZ1txlqKQbl1skKWXdFxjh6iigmNffqOqjmpdcpqumHJaZYusn3dYWHmjpIJeVGuZq5FpVGCNr6B3V1Z8qK2JX09nmbGYb1NciaylgWBXdp6tlXJZY4ippoZkWXKYqZNxWWKFop2CZVtylKWWe2BigZ6kj29db4+imHpfYYCan4tuXmyHmpeDaWF2j52TeV5kfZeei2tca4abmoJmYHSNoJl/ZmZ7kqCSeGNqgJigkHRja4OYnYtvZHCJmpmCamdzipiVf2podIuZkXtpZ3aLmo98bGp4jJmOe2xreoyZjXpqZ3ePnZB7bGp7kJmNfG5sfI+ZkYJwaXWGlpOEb2Rug5eYhm5iboeamYVwZ3CIl5uJdmlrgZCbjXxrZ3qMm5J/aWF0i5+ch25fboadnIZsXmyFm5yKcV5jfJCckntlYnaNoZuDaV9tiJ6hi25eZIGXo5R3ZGF5kaKdg2xjcY2fo45yY2iDlqCTd2RidIyalX9pYmyEk5aIdGpreYmSj4F0bW9/i5GJeW5sd4eRkYV5cnN+i5CNg3Zyd4aQj4x/dnl8hYmHh397e3yEhoeIfnh3eYGEjIp9dnF2gYqPhHVwcn2HiYuCenl2fICJkYZ7c256g4yMfXdxcn6Ci42DfnNzf4SRkoZ+dHN+hJGVh31ubnyFk5OGfHJwfISRlouBcm53g5GXiHxuanWAjZKKfW9nb32MlYd6bmhxfoqVjoN1a218iJSQhHducH2JkpGJfHBrdoSSlox8b253hI2QjYN6c3N+h46Rin52cXaEi5COf3Rub32JkI+EeHNwd4GHjI2Bd3FvfIaMkIh9dm5zgoqMi392cnWBh4iJhX15eXuFiIeIhX59e3mBgoSMhn19enp/f4GKh4SCeHR4fIKQjIaBdXN3eoWRi4iBcG90e4uTi4J3anF6f4qQh4N+cXJ2eoqYkIh7anB6gJCbjoZ6bHB3fIuVjYZ8b3J4eoaUj4h+dHN4eICOkJCIe25ucHuPmZOIeWttc32KlZKLgHNtcXeCkpaSh3hsbXN7h5KSiH9xaG11fpKZj4Z2Zmlyeo+dlox+a2p0e4mXloyDdm1yd32MmJiOgnNudHiAj5SPiX9ybnBxfY2RjYd2aWlqcoOSlpKHdWlqbnqMlZaShnVubnB9jpibloVzbW1vfo+ZnZiIdW5nZ3mLl52Ugm9oZ2p5ipKYkoFxa2VoeIuUm5eFc2pjZniOmKCcinhvZmNxiZOeoZF9b2NfbIOQmZ+TgHJoYmp9i5SblIZ3bGVnc4OQm56QfW9kY2x+j5qimYp6a2Roc4SUnaCXh3NlYWZ1ipagm454Z2Fja36Qm6CYhm9iYWRzhZGYm5ODcmZiY3SJmJ6floFtZWRqfJCcoJ+Te2hkY2h7jJadnI54ZV9eZ3uQm5yckHtuaGJmeI6do6GUfWhiYmZ2jZugo5mAa2JhY3CGl52knYZtXlZYaoOaoaSch21dWF1pgJWcoqGUfGRYW2R2j6CmpJ6KcmJgYG2EmaappZd+ZFlaYXSQo6elnYZpW1leZ36SnZ+fmoJpXFhXZoOapamlknRdVVZdb4ygp6yqlXZdT09bdZastLCnjG1VTFFgepSmr6yijG9USFBie5qus6ufhWlUTVVsgZeor6aXiXFaUFNjdo+ksa6finBYSk5leoyfqKWYindgUk9fdoaYpqyhkoJrWFNgdISVo6unmYt6ZlhYaHyKmqapn5GFcF9XXW+FkJifm5KDdmpgWl5sfoiTnJuVg3duYVtjd4mQmZ+dlIJ3b2ZgZnuNj5Wdm5B/eXZsZmt5hYeMlpmVhnx4bmRjbn2CipaZlo2AfHVpZGp9iI2YnpqRg3t5cGdlc4SKjpSYlYh7dW5mZG5/iYuNkZOOfnBuamRlcYOOkZKUkIV4cHRzcnZ+iZCPjpGNhH15fXp2d36Ij42JiouIgnl7e3Zzd36FhYOGg4GAeHh8d29xd39+f4WIhoSAen98eHV5g4WDgYOGh4mHiYh/dXR2f4OBhIiLj4qDhIB1cG90enyAiJCTk4yAgXptZ2dyhY2MjZGSkYR1dnhzcXF4g4qMjpCLhn10dHVvcHV9iIyKiYiFgHp2dHp8eHuBhYeDgoWFhYWEgH9/ent7fIGFiImHhoSEhH9/gXx7e3l8gYKEhIaEhIJ8e3x2eHh2fISGiIaAgIaCfHp8fH18AA=='
  ];
  return sound[soundNumber];
}

function createFavLink(attr) {
  // Create favicon link
  var link = document.createElement("link");
  link.type = attr['type'];
  link.rel = attr['rel'];
  link.href = attr['href'];
  return link;
}

function doSiren() {
  // Set siren favicon and play sound
  sirenFavicon = {
    'rel':'shortcut icon',
    'href':'data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAuLi7eHx8f/y0tLf84ODj/Pz8//0FBQf9HR0f/SkpK/z4+Pv82Njb/NDQ0/zg4OP84ODj/JCQk/yIiIv8yMjL/MzMz3SgoKP9FRUX/Tk5O/0RERP9HR0f/UFBQ/0BAQP81NTX/ODg4/0dHR/9LS0v/Q0ND/ywsLP8zMzP/MDAw/1RUVso+PkH/TExN/05OTv9FRUb/S0tL/0tLS/9GRkb/QEBA/z4+Pv9CQkL/QkJC/z8/P/86Ojr/REFB/2dnZ8rS0tRGSUl7/zg4Xv8+Pk3/NTU4/ygoKf8iIiL/R0dH/ycnKf8TExP/TU1NwBISEv8iHR3/Ui4u/2k7O//JyclQ8PDwFURElv8zM4v/NTVc/yoqLP8iIiL/Gxsb/1BQUP8wMDD/ERER/yEgIOsZEBD/TQ0N/2cYGP9pJCT/7u3tGfv7+wNeW6DgMzOY/zMzfv8pKUr/HR0k/xkZGf9XV1f/QEBA8BEREf8eGBjzSQMD/2YAAP9sAAD/cR8f/+7u7hX///8Aa2ul2TMzmf8yMpf/Jyd//zc3WP8pKTP/VVVV/T09PfBSUlLXSyws/28LC/92AAD/dgAA/41FReX5+fkG////AJWSwKAuLpj/Kiqc/46OxP+Zmav/gYGB/4WFhfJ1dXXlf39//66brv+qkJD/gQcH/3wAAP+UVlbU////AP///wCnp8Z5Cgqb/09PtP+ZmfL/q6vN/6qqqv99fX3/lJSU/7qmrP/YmrH/zre8/5o+Pv+KAAD/s39/oP///wD///8AtrbHZQYGpP9kZMT/mZn4/2Jd6P+hnrn/eHh4/4GBgf/bZW7/736B/+Soqv+jX1//lgAA/7GIiI7///8A////AOHf5jIaGqv/aGiz/5qa9P96dvP/lZGm/2NjY/9gYGD/yX6C/+uRlP/lpaX/oldX/5cEBP/WvLxl////AP///wDr6+sVMDCn9zw8o/+srND/mJjc/3Jygv9FRUX/UlJS/6KanP/Umqr/0rCx/5s0Nv+bFRX/1MjIVf///wD///8A////ADU1j+cDA5T/dnal/5KRrf9PS0//JiYm/ysrK/9lZWX/s5uu/6d1df+ZMTH/oEFB/+Xh4Sv///8A////AP///wByb7S1AACA/wICYv8cHD7/IyMj/yIiIv8nJyf/NTMz/1w5Of+MMzP/mTMz/6JOTvPz8/MM////AP///wD///8AkpKtoQICS/8ICC7/ICAh/yIiIv8iIiL/Kioq/zU1Nf9ENzf/aTQ0/4g1Nf+YWFjw////AP///wD///8A////AO/v8BmenqGHZWVl0Dc3N/8zMzP/OTk5/z4+Pv9EREb/SEhI/2hiYuqajY2s0czMT////wD///8AAAAAAAAAAAAAAAAAgAEAAIABAACAAQAAgAEAAIABAADAAQAAwAEAAMADAADAAwAAwAMAAMADAADAAwAA4AcAAA==',
    'type':'image/x-icon'
  };
  sirenFavgif = {
    'rel':'shortcut icon',
    'href':'data:image/gif;base64,R0lGODlhEAAQAPYAAP/MzP+Zmf9mZv8zZv8zM8zM/8zMzMyZzMyZmcxmZswzM5mZ/5mZzJmZmZlmmZlmZpkzM5kAAGZm/2ZmzGZmmWZmZmYzM2YAADMz/zMzzDMzmTMzZjMzMzMAAAAAmQAAZqoAAIgAAHcAAFUAAEQAABEAAAAAiAAAdwAAVQAARAAAIgAAEd3d3bu7u6qqqoiIiHd3d1VVVURERCIiIhEREf+ZzMxmmcwzZswAM5kAM2YzZrsAAAARAMyZ/5kzZgAAMyIAAJlmzGYz/2YzzGYzmTMA/zMAzDMAmTMAZgAAzAAA7gAAuwAAqgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/h1HaWZCdWlsZGVyIDAuMiBieSBZdmVzIFBpZ3VldAAh+QQFCgA1ACwAAAAADwAQAAAGpsBazUKjzY4c2WYpFIpGneOMw9loNM1aJPSAIRtXzawZeSBesumrdX0JZaBWACGrcFoLg4chDIAQAQMVgxkLLR4eQhARAQEtDTIvE3keJooRCAiPMjAtjycniosvMTMNFTENoKE1EBAWI0UNRSgfJx8zNK8WuaZFNLUfMi4WFhxSML/AGy+kMjLGHKW9UxQzMjHPMdnazzIcRt7YVEnfUlNU3uRUR0EAIfkEBQoAAwAsAAAAAA8AEAAABprAwUCxy0GOR8VNIRTuQBFk8nZrDqChYyKBVMyaoJAIst0iX0KZ+FI253IJIWKdQBC2olwEJ4RERCMJAXcJIiERIEI5ISFsZRAiIohCChAhFhZmJBeHiUNIF4EkI4YROzM0CpWQIgkjF5JvFgiqlUYPsIwQCggBqhCYHDPDMxa7AjMcMTIyMcvMzzLCHMzVHNfMwqjD19TZydpBACH5BAUKAAAALAAAAAAPABAAAAe4gAAAFBsbHIcziTRAgoIaGhsyiDMdJBeNAI8aLhyJHA0hETONjwYMMjMxDQgGERWCFR4NDAwGFRUtAQgQCIITHi0MEj23BgEBDwqOJi60LTEtLAgBECCCJtgtLS8cLQYICBER1yYnL6ovAC8NEO0ANOUnKCs0LzQ0MxbtNBwnJx/3aDTAl0hfjAqFAtKoQJCDDAsvCiWaGGOGQxkxYrywGEMGxhiHMHqUZPGix0MXO01EGRKRyhmBAAAh+QQBCgAAACwAAAAADwAQAAAHqIAAABlGRhmHiIeCgklGS4gaGoiLAEtLR5ITQ5GSM4tLTEcZE6QTnAyCMaAeoxKkGxpHE4IMTEysEhikHyYeR4IZR0esE7pEHycmTMCYHkOkQRrIysCRHhoTRBoo08uDGZwnEx/kvUwzNIecJhMn7r1HGwxDQ+qSJvgmkkHz2hscAGfMSAErg4QZHGLIkBEjBoeFCmUApPFw4UKAFTkIpCEQo0WMGmcEAgA7',
    'type':'image/gif'
  };
  document.head.appendChild(createFavLink(sirenFavicon));
  document.head.appendChild(createFavLink(sirenFavgif));

  if (sirenSound > 0){
      siren = document.createElement('audio');
      siren.setAttribute('src',loadSound(4));
      siren.load();
      siren.play();

  }
}

function isAd(link) {
  // Checks for ad links.
  var img = link.getElementsByTagName('img');
  if (img[0]) {
    // link contains image
    link.setAttribute('Ad', 'true');
    return true;
  }
  else if (link.innerHTML == 'Advertisement') {
    // Link text is 'Advertisement'
    link.setAttribute('Ad', 'true');
    return true;
  }
  else if (link.getAttribute('target') == '_blank'){
    // Link target is new window
    link.setAttribute('Ad', 'true');
    return true;
  }
  else {
    return false;
  }
}

/**********************
 * Helper Functions
 *********************/

function hash(str){
  // Hashes URLs
  var hash = 0, i, ch;
  if (str.length == 0) return hash;
  for (i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i);
    hash = ((hash<<5)-hash)+ch;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

function CSVToArray(strData, returnIntegers) {
  // Converts CSV string to array
  // Check to see if the delimiter is defined. If not, then default to comma.
  strDelimiter = ",";
  // Create a regular expression to parse the CSV values.
  var objPattern = new RegExp(
    (
      // Delimiters.
      "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
      // Quoted fields.
      "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
      // Standard fields.
      "([^\"\\" + strDelimiter + "\\r\\n]*))"
      ),
    "gi"
    );
  // Create an array to hold our data. Give the array a default empty first row.
  var arrData = [[]];
  // Create an array to hold our individual pattern matching groups.
  var arrMatches = null;
  // Keep looping over the regular expression matches until we can no longer find a match.
  while (arrMatches = objPattern.exec( strData )){
    // Get the delimiter that was found.
    var strMatchedDelimiter = arrMatches[ 1 ];
    // Check to see if the given delimiter has a length (is not the start of string) and if it matches
    // field delimiter. If id does not, then we know that this delimiter is a row delimiter.
    if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
      // Since we have reached a new row of data, add an empty row to our data array.
      arrData.push( [] );
    }
    // Now that we have our delimiter out of the way, let's check to see which kind of value we captured (quoted or unquoted).
    if (arrMatches[ 2 ]){
      // We found a quoted value. When we capture this value, unescape any double quotes.
      var strMatchedValue = arrMatches[ 2 ].replace(
        new RegExp( "\"\"", "g" ),
        "\""
        );
    } else {
      // We found a non-quoted value.
      var strMatchedValue = arrMatches[ 3 ];
    }
    // Now that we have our value string, let's add it to the data array.
    if (returnIntegers == true) {
      arrData[ arrData.length - 1 ].push(parseInt(strMatchedValue));
    } else {
      arrData[ arrData.length - 1 ].push(strMatchedValue);
    }
  }
  // Return the parsed data.
  return( arrData[0] );
}

/**********************************************
 * Changelog
 *********************************
 * 1.4 (tbd):
 *
 * Added: Links open in new tab
 * Added: Options Menu
 * Fixed: All links no longer highlighted on first load or incomplete/missing cookie
 * Added: Track settings cookie version and reset if format changes between versions
 *
 * -------------------------------
 * 1.3.2 (2013-03-28):
 *
 * Bugfix: Countdown div not available to all functions
 *
 * -------------------------------
 * 1.3.1 (2013-03-14):
 *
 * Bugfix: Countdown won't reset when clicked
 *
 * -------------------------------
 * 1.3 (2013-03-13):
 *
 * Added audio notification of new links
 * Added siren headline detection
 * Improved ad detection
 *
 * -------------------------------
 * 1.2 (2013-02-20):
 *
 * Added highlight new links
 * Added new links count to title
 * Added visited link style
 * Bug fixes
 *
 * -------------------------------
 * 1.1 (2013-02-15):
 *
 * Added Chrome and Opera compatibility
 *
 * -------------------------------
 * 1.0 (2013-02-14):
 *
 * Initial release
 *
 **********************************************/
